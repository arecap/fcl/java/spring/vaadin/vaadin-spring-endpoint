package com.vaadin.spring.endpoint.util;


import com.vaadin.ui.Component;
import com.vaadin.ui.HasComponents;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Iterator;

public final class ComponentUtil {

    private static final Logger LOGGER = LoggerFactory
            .getLogger(ComponentUtil.class);

    public static Component findComponentById(HasComponents root, String id) {
        LOGGER.debug("ComponentUtil findComponentById [{}] called on" +
                        " HasComponents root subclass [{}]",
                id, root.getClass());
        Iterator<Component> iterate = root.iterator();
        while (iterate.hasNext()) {
            Component c = iterate.next();
            if (id.equals(c.getId())) {
                return c;
            }
            if (c instanceof HasComponents) {
                Component cc = findComponentById((HasComponents) c, id);
                if (cc != null)
                    return cc;
            }
        }
        return null;
    }

}

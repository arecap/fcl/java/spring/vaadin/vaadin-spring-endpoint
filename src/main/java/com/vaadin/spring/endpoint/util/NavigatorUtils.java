package com.vaadin.spring.endpoint.util;

import com.vaadin.server.VaadinServlet;
import com.vaadin.spring.endpoint.annotation.GuiController;
import com.vaadin.ui.UI;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.core.annotation.AnnotatedElementUtils;
import org.springframework.core.annotation.AnnotationAttributes;
import org.springframework.util.Assert;

import java.util.LinkedHashSet;
import java.util.Set;

public final class NavigatorUtils {

    private static Logger logger = LoggerFactory
            .getLogger(NavigatorUtils.class);

    public static String resolvePath(String path) {
        return path.contains("{") ? path.substring(0, path.indexOf("{")) : path;
    }

    public static String getControllerUrl(Class<?> endpoint, Object ... params) {
        AnnotationAttributes aa = AnnotatedElementUtils
                .getMergedAnnotationAttributes(endpoint, GuiController.class);
        Assert.notNull(aa, "Support only @VaadinController annotated types!");
        StringBuilder pathParameter = new StringBuilder("");
        if(params.length >  0) {
            pathParameter.append("/");
            for(Object param: params) {
                pathParameter.append(param.toString()).append("/");
            }
        }
        return VaadinServlet.getCurrent().getServletContext().getContextPath()
                + NavigatorUtils.resolvePath(aa.getString("uri"))
                + (pathParameter.length() > 0 ? pathParameter.substring(0, pathParameter.length()-1) : "");
    }

    public static void navigateTo(Class<?> endpoint, Object ... params) {
        UI.getCurrent().getPage().setLocation(getControllerUrl(endpoint, params));
    }

    public static Set<String> getVaadinControllerUris(ApplicationContext applicationContext) {
        Set<String> controllerSectionUris = new LinkedHashSet<>();
        // more checks are performed by the UI provider
        final String[] uiBeanNames = applicationContext
                .getBeanNamesForAnnotation(GuiController.class);
        for (String uiBeanName : uiBeanNames) {
            AnnotationAttributes aa = AnnotatedElementUtils
                    .getMergedAnnotationAttributes(applicationContext.getType(uiBeanName), GuiController.class);
            controllerSectionUris.add(applicationContext.getEnvironment()
                    .resolvePlaceholders(NavigatorUtils.resolvePath(aa.getString("uri")))
                    .replaceFirst("^/", ""));
        }
        return controllerSectionUris;
    }

    public static String getVaadinControllerBean(ApplicationContext applicationContext, String uri) {
        final String[] uiBeanNames = applicationContext
                .getBeanNamesForAnnotation(GuiController.class);
        for (String uiBeanName : uiBeanNames) {
            AnnotationAttributes aa = AnnotatedElementUtils
                    .getMergedAnnotationAttributes(applicationContext.getType(uiBeanName), GuiController.class);
            if(applicationContext.getEnvironment()
                    .resolvePlaceholders(NavigatorUtils.resolvePath(aa.getString("uri")))
                    .replaceFirst("^/", "")
                    .equalsIgnoreCase(uri.replaceFirst("^/", ""))) {
                return uiBeanName;
            }
        }
        return null;
    }

    public static String[] getVaadinControllerBeanAndParams(ApplicationContext applicationContext, String path) {
        String[] beanAndParams = new String[2];
        beanAndParams[0] = getVaadinControllerBean(applicationContext, path);
        if(beanAndParams[0] != null)
            return beanAndParams;
        // then look for prefix matches
        int lastSlash = -1;
        String pathPart = path.replaceFirst("^/", "");
        StringBuilder parameters = new StringBuilder();
        while ((lastSlash = pathPart.lastIndexOf('/')) > -1) {
            pathPart = pathPart.substring(0, lastSlash);
            parameters.insert(0, path.substring(lastSlash + 1));
            logger.trace("Checking if [{}] is a valid UiSection", pathPart);
            beanAndParams[0] = getVaadinControllerBean(applicationContext, pathPart + "/");
            if(beanAndParams[0] != null) {
                beanAndParams[1] = parameters.toString();
                return beanAndParams;
            }
        }
        return beanAndParams;
    }

    public static Class<? extends UI> getVaadinControllerUriUiClass(ApplicationContext applicationContext, String uri) {
        final String[] uiBeanNames = applicationContext
                .getBeanNamesForAnnotation(GuiController.class);
        for (String uiBeanName : uiBeanNames) {
            AnnotationAttributes aa = AnnotatedElementUtils
                    .getMergedAnnotationAttributes(applicationContext.getType(uiBeanName), GuiController.class);
            if(applicationContext.getEnvironment()
                    .resolvePlaceholders(NavigatorUtils.resolvePath(aa.getString("uri")))
                    .replaceFirst("^/", "")
                    .equalsIgnoreCase(uri.replaceFirst("^/", ""))) {
                return aa.getClass("ui");
            }
        }
        return null;
    }



    public static Class<? extends UI> getVaadinControllerPathUiClass(ApplicationContext applicationContext, String path) {
        Class<? extends UI> uiClass = getVaadinControllerUriUiClass(applicationContext, path);
        if(uiClass != null)
            return uiClass;
        // then look for prefix matches
        int lastSlash = -1;
        String pathPart = path.replaceFirst("^/", "");
        StringBuilder parameters = new StringBuilder();
        while ((lastSlash = pathPart.lastIndexOf('/')) > -1) {
            pathPart = pathPart.substring(0, lastSlash);
            parameters.insert(0, pathPart.substring(lastSlash));
            logger.trace("Checking if [{}] is a valid UiSection", pathPart);
            uiClass = getVaadinControllerUriUiClass(applicationContext, pathPart+"/");
            if(uiClass != null) {
                return uiClass;
            }
        }
        return uiClass;
    }

//    public static Map<String, String> getVaadinControllerUriVariables()

}

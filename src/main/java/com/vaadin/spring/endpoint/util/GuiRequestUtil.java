/*
 * Copyright 2017 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.vaadin.spring.endpoint.util;

import com.vaadin.server.VaadinRequest;
import com.vaadin.shared.ui.ui.UIConstants;

public final class GuiRequestUtil {

    public static final String LOCATION_PARAMETER = "v-loc";

    public static final String CLIENT_WIDTH_PARAMETER = "v-cw";

    public static final String CLIENT_HEIGHT_PARAMETER = "v-ch";

    public static final String WINDOW_NAME_PARAMETER = "v-wn";

    public static final String THEME_PARAMETER = "theme";

    public static boolean isEventRequest(VaadinRequest vaadinRequest) {
        return "POST".equals(vaadinRequest.getMethod()) &&
                vaadinRequest.getParameter(UIConstants.UI_ID_PARAMETER) != null;
    }


}

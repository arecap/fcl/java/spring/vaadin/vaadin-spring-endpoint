/*
 * Copyright 2017 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.vaadin.spring.endpoint.event;


import com.vaadin.spring.endpoint.annotation.EventMapping;
import com.vaadin.ui.HasComponents;
import org.springframework.core.annotation.AnnotationUtils;
import org.springframework.util.ReflectionUtils;

import java.lang.reflect.Method;

/**
 *
 * @author Octavian Stirbei
 * @since 1.0
 *
 */
public interface EventMappingResolver {


    void resolveEventMapping(HasComponents layout, Object uiSection);

    /**
     * MethodFilter that matches {@link EventMapping @ListenerMapping} methods.
     */
    ReflectionUtils.MethodFilter EVENT_MAPPING_METHODS = new ReflectionUtils.MethodFilter() {
        @Override
        public boolean matches(Method method) {
            return AnnotationUtils.findAnnotation(method, EventMapping.class) != null;
        }
    };

}

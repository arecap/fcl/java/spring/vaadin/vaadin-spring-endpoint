/*
 * Copyright 2017 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.vaadin.spring.endpoint.event;


import com.vaadin.spring.endpoint.annotation.EventMapping;
import org.springframework.core.annotation.AnnotationUtils;
import org.springframework.util.ReflectionUtils;

import java.lang.reflect.Method;
import java.util.EventObject;

/**
 *
 * @author Octavian Stirbei
 * @since 1.0
 *
 */
public interface EventMappingDispatcher {

    String DISPATCHER_NAME = "dispatchEvent";

    void dispatchEvent(EventObject eventObject);

    class EventMethodsFilter implements ReflectionUtils.MethodFilter {

        private final Class<? extends EventObject> eventType;

        private final String section;

        public EventMethodsFilter(Class<? extends EventObject> eventType, String section) {
            this.eventType = eventType;
            this.section = section;
        }

        @Override
        public boolean matches(Method method) {
            EventMapping eventMapping = AnnotationUtils.getAnnotation(method, EventMapping.class);
            return eventMapping != null &&
                    this.eventType.isAssignableFrom(eventMapping.event()) &&
                    section.equalsIgnoreCase(eventMapping.section());
        }

    }

}

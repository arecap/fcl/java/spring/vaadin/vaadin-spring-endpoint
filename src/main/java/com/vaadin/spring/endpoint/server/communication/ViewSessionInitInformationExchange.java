/*
 * Copyright 2015-2017 The original authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.vaadin.spring.endpoint.server.communication;

import com.vaadin.navigator.View;
import com.vaadin.server.VaadinRequest;
import com.vaadin.server.communication.ServletUIInitHandler;
import com.vaadin.spring.annotation.ViewScope;
import com.vaadin.spring.endpoint.annotation.GuiApplication;
import com.vaadin.ui.UI;
import com.vaadin.util.CurrentInstance;
import org.contextualj.lang.annotation.expression.SourceName;
import org.contextualj.lang.annotation.expression.SourceType;
import org.contextualj.lang.annotation.pointcut.HandlingReturn;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cop.annotation.ContextOriented;
import org.springframework.core.annotation.AnnotationUtils;

import static com.vaadin.server.communication.UIInitHandler.BROWSER_DETAILS_PARAMETER;
import static com.vaadin.spring.endpoint.util.GuiRequestUtil.*;

@ContextOriented
@SourceType(View.class)
@ViewScope
public class ViewSessionInitInformationExchange {

    @Autowired
    private UiInitInformationExchange uiInitInformationExchange;

    @HandlingReturn
    @SourceName("enter")
    public void setUiInitInformationExchange() {
        VaadinRequest request = CurrentInstance.get(VaadinRequest.class);
        UI ui = UI.getCurrent();
        if(ServletUIInitHandler.isUIInitRequest(request)) {
            uiInitInformationExchange.setUiInitSupported(AnnotationUtils.findAnnotation(ui.getClass(), GuiApplication.class) != null);
            uiInitInformationExchange.setLocation(request.getParameter(LOCATION_PARAMETER));
            uiInitInformationExchange.setPathInfo(request.getPathInfo());
            uiInitInformationExchange.setClientWidth(request.getParameter(CLIENT_WIDTH_PARAMETER));
            uiInitInformationExchange.setClientHeight(request.getParameter(CLIENT_HEIGHT_PARAMETER));
            uiInitInformationExchange.setWindowName(request.getParameter(WINDOW_NAME_PARAMETER));
            uiInitInformationExchange.setTheme(request.getParameter(THEME_PARAMETER));
            uiInitInformationExchange.setBrowserDetails(request.getParameter(BROWSER_DETAILS_PARAMETER));
            uiInitInformationExchange.setEmbeddedId(ui.getEmbedId());
            uiInitInformationExchange.setCsrfToken(ui.getSession().getCsrfToken());
            uiInitInformationExchange.setLastProcessedClientToServerId(ui.getLastProcessedClientToServerId());
            uiInitInformationExchange.setConnectorsCount(ui.getConnectorTracker().getDirtyConnectors().size());
        }
    }

}

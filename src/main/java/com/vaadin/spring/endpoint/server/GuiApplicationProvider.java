/*
 * Copyright 2017 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.vaadin.spring.endpoint.server;

import com.vaadin.server.UIClassSelectionEvent;
import com.vaadin.server.VaadinRequest;
import com.vaadin.server.VaadinSession;
import com.vaadin.spring.endpoint.util.NavigatorUtils;
import com.vaadin.spring.server.SpringUIProvider;
import com.vaadin.ui.UI;

public class GuiApplicationProvider extends SpringUIProvider {

    public GuiApplicationProvider(VaadinSession vaadinSession) {
        super(vaadinSession);
    }

    @Override
    public Class<? extends UI> getUIClass(UIClassSelectionEvent uiClassSelectionEvent) {
        Class<? extends UI> uiClass = super.getUIClass(uiClassSelectionEvent);
        return uiClass == null ? extractUIPathFromRequest(uiClassSelectionEvent.getRequest()) : uiClass;
    }

    public Class<? extends UI> extractUIPathFromRequest(VaadinRequest request) {
        return NavigatorUtils.getVaadinControllerPathUiClass(getWebApplicationContext(), request.getPathInfo());
    }

}

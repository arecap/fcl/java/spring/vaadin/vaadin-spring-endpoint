/*
 * Copyright 2017 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.vaadin.spring.endpoint.server.communication;

import com.vaadin.server.VaadinRequest;
import com.vaadin.server.VaadinService;
import com.vaadin.server.WrappedSession;

import javax.servlet.http.Cookie;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.security.Principal;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import static com.vaadin.server.communication.UIInitHandler.BROWSER_DETAILS_PARAMETER;
import static com.vaadin.spring.endpoint.util.GuiRequestUtil.*;

public final class EndpointVaadinRequest implements VaadinRequest {

    private final VaadinRequest request;

    private static final Map<String, String> uiRequestParameters = new HashMap<String, String>();

    public EndpointVaadinRequest(VaadinRequest request, UiInitInformationExchange uiInitRequest) {
        this.request = request;
        uiRequestParameters.put(LOCATION_PARAMETER, uiInitRequest.getLocation());
        uiRequestParameters.put(CLIENT_WIDTH_PARAMETER, uiInitRequest.getClientWidth());
        uiRequestParameters.put(CLIENT_HEIGHT_PARAMETER, uiInitRequest.getClientHeight());
        uiRequestParameters.put(WINDOW_NAME_PARAMETER, uiInitRequest.getWindowName());
        uiRequestParameters.put(THEME_PARAMETER, uiInitRequest.getTheme());
        uiRequestParameters.put(BROWSER_DETAILS_PARAMETER, uiInitRequest.getBrowserDetails());
    }


    @Override
    public String getParameter(String s) {
        return uiRequestParameters.containsKey(s) ? uiRequestParameters.get(s) : request.getParameter(s);
    }

    @Override
    public Map<String, String[]> getParameterMap() {
        return request.getParameterMap();
    }

    @Override
    public int getContentLength() {
        return request.getContentLength();
    }

    @Override
    public InputStream getInputStream() throws IOException {
        return request.getInputStream();
    }

    @Override
    public Object getAttribute(String s) {
        return request.getAttribute(s);
    }

    @Override
    public void setAttribute(String s, Object o) {
        request.setAttribute(s, o);
    }

    @Override
    public String getPathInfo() {
        return request.getPathInfo();
    }

    @Override
    public String getContextPath() {
        return request.getContextPath();
    }

    @Override
    public WrappedSession getWrappedSession() {
        return request.getWrappedSession();
    }

    @Override
    public WrappedSession getWrappedSession(boolean b) {
        return request.getWrappedSession(b);
    }

    @Override
    public String getContentType() {
        return request.getContentType();
    }

    @Override
    public Locale getLocale() {
        return request.getLocale();
    }

    @Override
    public String getRemoteAddr() {
        return request.getRemoteAddr();
    }

    @Override
    public boolean isSecure() {
        return request.isSecure();
    }

    @Override
    public String getHeader(String s) {
        return request.getHeader(s);
    }

    @Override
    public VaadinService getService() {
        return request.getService();
    }

    @Override
    public Cookie[] getCookies() {
        return request.getCookies();
    }

    @Override
    public String getAuthType() {
        return request.getAuthType();
    }

    @Override
    public String getRemoteUser() {
        return request.getRemoteUser();
    }

    @Override
    public Principal getUserPrincipal() {
        return request.getUserPrincipal();
    }

    @Override
    public boolean isUserInRole(String s) {
        return request.isUserInRole(s);
    }

    @Override
    public void removeAttribute(String s) {
        request.removeAttribute(s);
    }

    @Override
    public Enumeration<String> getAttributeNames() {
        return request.getAttributeNames();
    }

    @Override
    public Enumeration<Locale> getLocales() {
        return request.getLocales();
    }

    @Override
    public String getRemoteHost() {
        return request.getRemoteHost();
    }

    @Override
    public int getRemotePort() {
        return request.getRemotePort();
    }

    @Override
    public String getCharacterEncoding() {
        return request.getCharacterEncoding();
    }

    @Override
    public BufferedReader getReader() throws IOException {
        return request.getReader();
    }

    @Override
    public String getMethod() {
        return request.getMethod();
    }

    @Override
    public long getDateHeader(String s) {
        return request.getDateHeader(s);
    }

    @Override
    public Enumeration<String> getHeaderNames() {
        return request.getHeaderNames();
    }

    @Override
    public Enumeration<String> getHeaders(String s) {
        return request.getHeaders(s);
    }
}

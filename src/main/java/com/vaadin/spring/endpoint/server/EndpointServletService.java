/*
 * Copyright 2017 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.vaadin.spring.endpoint.server;

import com.vaadin.server.*;
import com.vaadin.spring.endpoint.server.communication.EndpointUiInitHandler;
import com.vaadin.spring.endpoint.server.communication.EndpointVaadinSession;
import com.vaadin.spring.endpoint.util.GuiRequestUtil;
import com.vaadin.spring.server.SpringVaadinServletService;

import java.util.List;

public class EndpointServletService extends SpringVaadinServletService {

    /**
     * Create a servlet service instance that allows the use of a custom service
     * URL.
     *
     * @param servlet
     * @param deploymentConfiguration
     * @param serviceUrl              custom service URL to use (relative to context path, starting
     *                                with a slash) or null for default
     * @throws ServiceException
     */
    public EndpointServletService(VaadinServlet servlet, DeploymentConfiguration deploymentConfiguration, String serviceUrl) throws ServiceException {
        super(servlet, deploymentConfiguration, serviceUrl);
    }

    @Override
    protected List<RequestHandler> createRequestHandlers() throws ServiceException {
        List<RequestHandler> handlers =  super.createRequestHandlers();
        handlers.add(new EndpointUiInitHandler());
        return handlers;
    }

    @Override
    protected boolean requestCanCreateSession(VaadinRequest request) {
        if(!super.requestCanCreateSession(request)) {
            return GuiRequestUtil.isEventRequest(request);
        }
        return true;
    }

    @Override
    protected VaadinSession createVaadinSession(VaadinRequest request) throws ServiceException {
        return new EndpointVaadinSession(this);
    }

}

/*
 * Copyright 2017 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.vaadin.spring.endpoint.server.communication;

import com.vaadin.server.*;
import com.vaadin.server.communication.UIInitHandler;
import com.vaadin.shared.communication.PushMode;
import com.vaadin.shared.ui.ui.Transport;
import com.vaadin.shared.ui.ui.UIConstants;
import com.vaadin.spring.endpoint.server.GuiApplicationProvider;
import com.vaadin.spring.endpoint.util.GuiRequestUtil;
import com.vaadin.spring.endpoint.util.NavigatorUtils;
import com.vaadin.spring.server.SpringVaadinServletService;
import com.vaadin.ui.UI;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.context.WebApplicationContext;

import java.io.IOException;

public class EndpointUiInitHandler extends UIInitHandler {

    //TODO
    private static Logger logger = LoggerFactory
            .getLogger(EndpointUiInitHandler.class);


    private transient WebApplicationContext webApplicationContext = null;

    private transient VaadinSession vaadinSession = null;

    @Override
    public boolean synchronizedHandleRequest(VaadinSession session, VaadinRequest request, VaadinResponse response) throws IOException {
        vaadinSession = session;
        UI ui = session.getService().findUI(request);
        UiInitInformationExchange uiInitInfo = getUiInitInformationExchange();
        uiInitInfo.setLastProcessedClientToServerId(uiInitInfo.getLastProcessedClientToServerId() + 1);
        if(shouldBindInformationExchange(ui, uiInitInfo)) {
            logger.debug("Session should bind information exchange ui embedded [{}] lastProcessedClientToServerId [{}]." +
                    "Session ui embedded [{}] lastProcessedClientToServerId [{}]",
                    uiInitInfo.getEmbeddedId(), uiInitInfo.getLastProcessedClientToServerId(),
                    ui != null ? ui.getEmbedId() : null, ui != null ? ui.getLastProcessedClientToServerId() : null);
            if(EndpointVaadinSession.class.isAssignableFrom(session.getClass())) {
                ((EndpointVaadinSession)session).setUseSessionCsrfToken(false);
            }
            bindInformationExchange(session, new EndpointVaadinRequest(request, uiInitInfo), ui, uiInitInfo);
        }
        return false;
    }

    @Override
    protected boolean isInitRequest(VaadinRequest vaadinRequest) {
        return GuiRequestUtil.isEventRequest(vaadinRequest);
    }

    protected WebApplicationContext getWebApplicationContext() {
        if (webApplicationContext == null) {
            webApplicationContext = ((SpringVaadinServletService) vaadinSession
                    .getService()).getWebApplicationContext();
        }
        return webApplicationContext;
    }

    protected UiInitInformationExchange getUiInitInformationExchange() {
        return  getWebApplicationContext().getBean(UiInitInformationExchange.class);
    }

    protected boolean shouldBindInformationExchange(UI ui, UiInitInformationExchange uiInitRequest) {
        if(uiInitRequest == null || !uiInitRequest.isUiInitSupported())
            return false;
        if(ui == null)
            return true;
        //TODO jwt informational exchange contains all cookie data
//        if(!uiInitRequest.getEmbeddedId().equalsIgnoreCase(ui.getEmbedId()))
//            return true;
        return ui.getLastProcessedClientToServerId() > 0
                && uiInitRequest.getLastProcessedClientToServerId() != ui.getLastProcessedClientToServerId() + 1;
    }

    protected void bindInformationExchange(VaadinSession session, VaadinRequest request, UI ui, UiInitInformationExchange uiInitInfo) throws IOException {
        initSessionConnectorSequence(session, request, uiInitInfo);
        bindSessionUi(session, request, ui, uiInitInfo);
    }

    private void initSessionConnectorSequence(VaadinSession session, VaadinRequest request, UiInitInformationExchange uiInitInfo) {
        int firstConnectorId = Integer.parseInt(request.getParameter(UIConstants.UI_ID_PARAMETER)) * uiInitInfo.getConnectorsCount();
        logger.debug("Prepare session browser details and connector sequence id [{}]. " +
                "UI connectors count [{}] ", firstConnectorId, uiInitInfo.getConnectorsCount());
        session.getBrowser().updateRequestDetails(request);
        for(int i=0;i < firstConnectorId; i++) {
            session.createConnectorId(null);
        }
    }

    private void bindSessionUi(VaadinSession session, VaadinRequest request, UI ui, UiInitInformationExchange uiInitInfo) throws IOException {
         if(ui == null) {
            ui = createUiContext(request, session, uiInitInfo);
            getInitialUidl(request, ui);
        }
        logger.debug("Configure ui connector tracker sync id [{}] " +
                "from Information Exchange ui lastProcessedClientToServerId [{}]",
                ui.getConnectorTracker().getCurrentSyncId(),  uiInitInfo.getLastProcessedClientToServerId() - 1);
        for(int i = 0; i < uiInitInfo.getLastProcessedClientToServerId(); i++) {
            ui.getConnectorTracker().setWritingResponse(true);
            ui.getConnectorTracker().setWritingResponse(false);
        }
        ui.setLastProcessedClientToServerId(uiInitInfo.getLastProcessedClientToServerId() - 1);
        logger.debug("Setup complete! ui connector tracker sync id [{}] ui lastProcessedClientToServerId [{}]",
                ui.getConnectorTracker().getCurrentSyncId(), ui.getLastProcessedClientToServerId());
        for(int i=0; i< Integer.parseInt(request.getParameter(UIConstants.UI_ID_PARAMETER)) + 1; i++) {
            session.getNextUIid();
        }
        UI.getCurrent().getConnectorTracker().markAllConnectorsDirty();
    }

    private UI createUiContext(VaadinRequest request, VaadinSession session, UiInitInformationExchange uiInitInfo) {
        int uiId = Integer.parseInt(request.getParameter(UIConstants.UI_ID_PARAMETER));
        logger.debug("Session bind new UiContext for request uiId [{}]", uiId);
        Class<? extends UI> uiClass = NavigatorUtils.getVaadinControllerPathUiClass(getWebApplicationContext(),
                uiInitInfo.getPathInfo());
        UIProvider provider = new GuiApplicationProvider(session);
        UICreateEvent event = new UICreateEvent(request, uiClass, uiId);
        UI uiContext = uiClass.cast(provider.createInstance(event));
        // Initialize some fields for a newly created UI
        if (uiContext.getSession() != session) {
            // Session already set for LegacyWindow
            uiContext.setSession(session);
        }
        PushMode pushMode = provider.getPushMode(event);
        if (pushMode == null) {
            pushMode = session.getService().getDeploymentConfiguration()
                    .getPushMode();
        }
        uiContext.getPushConfiguration().setPushMode(pushMode);
        Transport transport = provider.getPushTransport(event);
        if (transport != null) {
            uiContext.getPushConfiguration().setTransport(transport);
        }
        // Set thread local here so it is available in init
        UI.setCurrent(uiContext);
        uiContext.doInit(request, uiId, uiInitInfo.getEmbeddedId());
        session.addUI(uiContext);
        return uiContext;
    }


}

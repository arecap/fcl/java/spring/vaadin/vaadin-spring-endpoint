/*
 * Copyright 2017 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.vaadin.spring.endpoint.server.communication;

import com.vaadin.server.VaadinService;
import com.vaadin.server.VaadinSession;
import com.vaadin.spring.server.SpringVaadinServletService;
import org.springframework.web.context.WebApplicationContext;

public final class EndpointVaadinSession extends VaadinSession {

    private transient WebApplicationContext webApplicationContext = null;

    private transient boolean useSessionCsrfToken = true;

    public EndpointVaadinSession(VaadinService service) {
        super(service);
    }

    public void setUseSessionCsrfToken(boolean useSessionCsrfToken) {
        this.useSessionCsrfToken = useSessionCsrfToken;
    }

    protected WebApplicationContext getWebApplicationContext() {
        if (webApplicationContext == null) {
            webApplicationContext = ((SpringVaadinServletService) this
                    .getService()).getWebApplicationContext();
        }

        return webApplicationContext;
    }

    protected UiInitInformationExchange getUiInitInformationExchange() {
        return  getWebApplicationContext().getBean(UiInitInformationExchange.class);
    }

    @Override
    public String getCsrfToken() {
        assert this.hasLock();
        if(useSessionCsrfToken)
            return super.getCsrfToken();
        return getUiInitInformationExchange().getCsrfToken();
    }
}

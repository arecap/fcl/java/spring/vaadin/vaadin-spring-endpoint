/*
 * Copyright 2017 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.vaadin.spring.endpoint.server.communication;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.vaadin.spring.annotation.ViewScope;
import org.springframework.web.exchange.annotation.InformationExchange;

@InformationExchange
@ViewScope
@JsonIgnoreProperties(ignoreUnknown = true)
public final class UiInitInformationExchange {

    @JsonProperty
    private boolean uiInitSupported;

    @JsonProperty
    private String location;

    @JsonProperty
    private String pathInfo;

    @JsonProperty
    private String clientWidth;

    @JsonProperty
    private String clientHeight;

    @JsonProperty
    private String windowName;

    @JsonProperty
    private String theme;

    @JsonProperty
    private String browserDetails;

    @JsonProperty
    private String embeddedId;

    @JsonProperty
    private String csrfToken;

    @JsonProperty
    private Integer lastProcessedClientToServerId;

    @JsonProperty
    private Integer connectorsCount;

    public boolean isUiInitSupported() {
        return uiInitSupported;
    }

    public void setUiInitSupported(boolean uiInitSupported) {
        this.uiInitSupported = uiInitSupported;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getPathInfo() {
        return pathInfo;
    }

    public void setPathInfo(String pathInfo) {
        this.pathInfo = pathInfo;
    }

    public String getClientWidth() {
        return clientWidth;
    }

    public void setClientWidth(String clientWidth) {
        this.clientWidth = clientWidth;
    }

    public String getClientHeight() {
        return clientHeight;
    }

    public void setClientHeight(String clientHeight) {
        this.clientHeight = clientHeight;
    }

    public String getWindowName() {
        return windowName;
    }

    public void setWindowName(String windowName) {
        this.windowName = windowName;
    }

    public String getTheme() {
        return theme;
    }

    public void setTheme(String theme) {
        this.theme = theme;
    }

    public String getBrowserDetails() {
        return browserDetails;
    }

    public void setBrowserDetails(String browserDetails) {
        this.browserDetails = browserDetails;
    }

    public String getEmbeddedId() {
        return embeddedId;
    }

    public void setEmbeddedId(String embeddedId) {
        this.embeddedId = embeddedId;
    }

    public String getCsrfToken() {
        return csrfToken;
    }

    public void setCsrfToken(String csrfToken) {
        this.csrfToken = csrfToken;
    }

    public Integer getLastProcessedClientToServerId() {
        return lastProcessedClientToServerId;
    }

    public void setLastProcessedClientToServerId(Integer lastProcessedClientToServerId) {
        this.lastProcessedClientToServerId = lastProcessedClientToServerId;
    }

    public Integer getConnectorsCount() {
        return connectorsCount;
    }

    public void setConnectorsCount(Integer connectorsCount) {
        this.connectorsCount = connectorsCount;
    }

}

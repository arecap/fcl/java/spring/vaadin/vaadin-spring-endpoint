package com.vaadin.spring.endpoint.server;

import com.vaadin.server.*;
import com.vaadin.spring.internal.UIScopeImpl;
import com.vaadin.spring.internal.VaadinSessionScope;
import com.vaadin.spring.server.SpringVaadinServlet;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

import javax.servlet.ServletException;
import java.util.ArrayList;
import java.util.List;

public class EndpointVaadinServlet extends SpringVaadinServlet {


    @Override
    protected void servletInitialized() throws ServletException {
        VaadinServletService service = getService();
        service.addSessionInitListener(new SessionInitListener() {

            private static final long serialVersionUID = -6307820453486668084L;

            @Override
            public void sessionInit(SessionInitEvent sessionInitEvent)
                    throws ServiceException {
                WebApplicationContext webApplicationContext = WebApplicationContextUtils
                        .getWebApplicationContext(getServletContext());

                // remove DefaultUIProvider instances to avoid mapping
                // extraneous UIs if e.g. a servlet is declared as a nested
                // class in a UI class
                VaadinSession session = sessionInitEvent.getSession();
                List<UIProvider> uiProviders = new ArrayList<UIProvider>(
                        session.getUIProviders());
                for (UIProvider provider : uiProviders) {
                    // use canonical names as these may have been loaded with
                    // different classloaders
                    if (DefaultUIProvider.class.getCanonicalName().equals(
                            provider.getClass().getCanonicalName())) {
                        session.removeUIProvider(provider);
                    }
                }

                // add Endpoint UI provider
                GuiApplicationProvider uiProvider = new GuiApplicationProvider(session);
                session.addUIProvider(uiProvider);
            }
        });
        service.addSessionDestroyListener(new SessionDestroyListener() {
            @Override
            public void sessionDestroy(SessionDestroyEvent event) {
                VaadinSession session = event.getSession();

                UIScopeImpl.cleanupSession(session);
                VaadinSessionScope.cleanupSession(session);
            }
        });
    }

    @Override
    protected VaadinServletService createServletService(DeploymentConfiguration deploymentConfiguration) throws ServiceException {
        // this is needed when using a custom service URL
        EndpointServletService service = new EndpointServletService(
                this, deploymentConfiguration, getServiceUrlPath());
        service.init();
        return service;
    }

}

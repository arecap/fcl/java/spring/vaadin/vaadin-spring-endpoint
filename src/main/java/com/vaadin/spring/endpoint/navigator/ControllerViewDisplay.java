package com.vaadin.spring.endpoint.navigator;

import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewDisplay;
import com.vaadin.spring.annotation.SpringViewDisplay;
import com.vaadin.spring.annotation.UIScope;
import com.vaadin.ui.Component;
import com.vaadin.ui.UI;
import org.springframework.util.Assert;

@SpringViewDisplay
@UIScope
public class ControllerViewDisplay implements ViewDisplay {

    @Override
    public void showView(View view) {
        Assert.isAssignable(Component.class, view.getClass());
        UI.getCurrent().setContent((Component) view);
    }

}

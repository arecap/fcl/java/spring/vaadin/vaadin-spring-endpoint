/*
 * Copyright 2017 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.vaadin.spring.endpoint.ui.method;


import org.contextualj.lang.weaving.MuxContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.aop.support.AopUtils;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.cop.scope.ContextObject;
import org.springframework.core.DefaultParameterNameDiscoverer;
import org.springframework.core.MethodIntrospector;
import org.springframework.core.ParameterNameDiscoverer;
import org.springframework.util.Assert;
import org.springframework.util.ReflectionUtils;
import org.springframework.web.bind.support.DefaultDataBinderFactory;
import org.springframework.web.bind.support.WebBindingInitializer;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;
import org.springframework.web.context.request.ServletWebRequest;
import org.springframework.web.method.HandlerMethod;
import org.springframework.web.method.annotation.ExpressionValueMethodArgumentResolver;
import org.springframework.web.method.annotation.RequestParamMapMethodArgumentResolver;
import org.springframework.web.method.annotation.RequestParamMethodArgumentResolver;
import org.springframework.web.method.support.HandlerMethodArgumentResolver;
import org.springframework.web.method.support.HandlerMethodArgumentResolverComposite;
import org.springframework.web.method.support.InvocableHandlerMethod;
import org.springframework.web.servlet.mvc.method.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

/**
 *
 * @author Octavian Stirbei
 * @since 1.0
 *
 */
public class MethodMappingService implements MethodMappingHandler, ApplicationContextAware, InitializingBean {

    private static Logger logger = LoggerFactory
            .getLogger(MethodMappingService.class);

    private ParameterNameDiscoverer parameterNameDiscoverer = new DefaultParameterNameDiscoverer();

    private HandlerMethodArgumentResolverComposite argumentResolvers;

    private WebBindingInitializer webBindingInitializer;

    private ApplicationContext applicationContext;

    /**
     * Configure the complete list of supported argument types thus overriding
     * the resolvers that would otherwise be configured by default.
     */
    public void setArgumentResolvers(List<HandlerMethodArgumentResolver> argumentResolvers) {
        if (argumentResolvers == null) {
            this.argumentResolvers = null;
        }
        else {
            this.argumentResolvers = new HandlerMethodArgumentResolverComposite();
            this.argumentResolvers.addResolvers(argumentResolvers);
        }
    }

    /**
     * Return the configured argument resolvers, or possibly {@code null} if
     * not initialized yet via {@link #afterPropertiesSet()}.
     */
    public List<HandlerMethodArgumentResolver> getArgumentResolvers() {
        return (this.argumentResolvers != null) ? this.argumentResolvers.getResolvers() : null;
    }

    /**
     * Provide a WebBindingInitializer with "global" initialization to apply
     * to every DataBinder instance.
     */
    public void setWebBindingInitializer(WebBindingInitializer webBindingInitializer) {
        this.webBindingInitializer = webBindingInitializer;
    }

    /**
     * Return the configured WebBindingInitializer, or {@code null} if none.
     */
    public WebBindingInitializer getWebBindingInitializer() {
        return this.webBindingInitializer;
    }

    /**
     * Set the ParameterNameDiscoverer to use for resolving method parameter names if needed
     * (e.g. for default attribute names).
     * <p>Default is a {@link org.springframework.core.DefaultParameterNameDiscoverer}.
     */
    public void setParameterNameDiscoverer(ParameterNameDiscoverer parameterNameDiscoverer) {
        this.parameterNameDiscoverer = parameterNameDiscoverer;
    }

    /**
     * A {@link WebApplicationContext} is expected for resolving expressions
     * in method argument default values.
     */
    @Override
    public void setApplicationContext(ApplicationContext applicationContext) {
        this.applicationContext = applicationContext;
    }

    /**
     * Return the owning factory of this bean instance, or {@code null} if none.
     */
    protected ConfigurableBeanFactory getBeanFactory() {
        BeanFactory beanFactory = applicationContext.getAutowireCapableBeanFactory();
        Assert.isAssignable(ConfigurableBeanFactory.class, beanFactory.getClass());
        return (ConfigurableBeanFactory) beanFactory;
    }


    @Override
    public void afterPropertiesSet() {
        if (this.argumentResolvers == null) {
            List<HandlerMethodArgumentResolver> resolvers = getDefaultArgumentResolvers();
            this.argumentResolvers = new HandlerMethodArgumentResolverComposite().addResolvers(resolvers);
        }
    }

    /**
     * Return the list of argument resolvers to use for {@code @UiMapping @EventMapping}
     * methods including built-in and custom resolvers.
     */
    private List<HandlerMethodArgumentResolver> getDefaultArgumentResolvers() {
        List<HandlerMethodArgumentResolver> resolvers = new ArrayList<HandlerMethodArgumentResolver>();

        // Annotation-based argument resolution
        resolvers.add(new RequestParamMethodArgumentResolver(getBeanFactory(), false));
        resolvers.add(new RequestParamMapMethodArgumentResolver());
        resolvers.add(new PathVariableMethodArgumentResolver());
        resolvers.add(new PathVariableMapMethodArgumentResolver());
        resolvers.add(new MatrixVariableMethodArgumentResolver());
        resolvers.add(new MatrixVariableMapMethodArgumentResolver());
        resolvers.add(new ExpressionValueMethodArgumentResolver(getBeanFactory()));
        resolvers.add(new SessionAttributeMethodArgumentResolver());
        resolvers.add(new RequestAttributeMethodArgumentResolver());

        // Type-based argument resolution
        resolvers.add(new ServletRequestMethodArgumentResolver());
        resolvers.add(new ServletResponseMethodArgumentResolver());

        // Catch-all
        resolvers.add(new RequestParamMethodArgumentResolver(getBeanFactory(), true));

        return resolvers;
    }

    @Override
    public void handleMethodMapping(Object target, ReflectionUtils.MethodFilter methodFilter) {
        Set<Method> uiMappings = new LinkedHashSet<>();
        uiMappings.addAll(MethodIntrospector.selectMethods(AopUtils.getTargetClass(target), methodFilter));
        if(MuxContext.class.isAssignableFrom(target.getClass())) {
            for(Object contextAdvice: ((MuxContext) target).getJoinPointChannels()) {
                if(ContextObject.class.isAssignableFrom(contextAdvice.getClass())) {
                    uiMappings
                            .addAll(MethodIntrospector.selectMethods(((ContextObject) contextAdvice).getTargetBeanType(), methodFilter));
                }
            }
        }
        if(uiMappings.size() == 0) {
            logger.debug("No @InitBinder declared in VaadinController bean type [{}] ", target.getClass());
        }
        for(Method uiMapping: uiMappings) {
            if(uiMapping.getDeclaringClass().isAssignableFrom(AopUtils.getTargetClass(target))) {
                service(((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getRequest(),
                        ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getResponse(),
                        createInitBinderMethod(target, uiMapping));
            } else {
                if(MuxContext.class.isAssignableFrom(target.getClass())) {
                    for(Object contextAdvice: ((MuxContext) target).getJoinPointChannels()) {
                        if(ContextObject.class.isAssignableFrom(contextAdvice.getClass())) {
                            if(uiMapping.getDeclaringClass().isAssignableFrom(((ContextObject) contextAdvice).getTargetBeanType())) {
                                service(((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getRequest(),
                                        ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getResponse(),
                                        createInitBinderMethod(((ContextObject) contextAdvice).getTargetObject(), uiMapping));
                            }
                        }
                    }
                }
            }
        }
    }

    protected void service(HttpServletRequest request, HttpServletResponse response, HandlerMethod handlerMethod) {
        ServletWebRequest webRequest = new ServletWebRequest(request, response);
        try {
            ((InvocableHandlerMethod)handlerMethod).invokeForRequest(webRequest, null);
        } catch (Exception e) {
            logger.debug("Exception service request", e);
        } finally {
            webRequest.requestCompleted();
        }
    }

    private InvocableHandlerMethod createInitBinderMethod(Object bean, Method method) {
        InvocableHandlerMethod binderMethod = new InvocableHandlerMethod(bean, method);
        binderMethod.setHandlerMethodArgumentResolvers(this.argumentResolvers);
        binderMethod.setDataBinderFactory(new DefaultDataBinderFactory(this.webBindingInitializer));
        binderMethod.setParameterNameDiscoverer(this.parameterNameDiscoverer);
        return binderMethod;
    }

}

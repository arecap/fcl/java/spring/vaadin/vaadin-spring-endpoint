/*
 * Copyright 2015-2017 The original authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.vaadin.spring.endpoint.ui.mvc;

import com.vaadin.server.AbstractClientConnector;
import com.vaadin.spring.annotation.ViewScope;
import com.vaadin.spring.endpoint.annotation.EventMapping;
import com.vaadin.spring.endpoint.event.EventMappingDispatcher;
import com.vaadin.spring.endpoint.event.EventMappingResolver;
import com.vaadin.spring.endpoint.event.method.ComponentMethodArgumentResolver;
import com.vaadin.spring.endpoint.navigator.GuiControllerService;
import com.vaadin.spring.endpoint.ui.method.MethodMappingHandler;
import com.vaadin.spring.endpoint.ui.method.MethodMappingService;
import com.vaadin.spring.endpoint.util.ComponentUtil;
import com.vaadin.spring.endpoint.util.NavigatorUtils;
import com.vaadin.ui.Component;
import com.vaadin.ui.HasComponents;
import org.contextualj.lang.annotation.ContextTarget;
import org.contextualj.lang.annotation.expression.SourceName;
import org.contextualj.lang.annotation.expression.SourceType;
import org.contextualj.lang.annotation.pointcut.Track;
import org.contextualj.lang.weaving.MuxContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.aop.support.AopUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.cop.annotation.ContextOriented;
import org.springframework.cop.scope.ContextObject;
import org.springframework.core.MethodIntrospector;
import org.springframework.core.annotation.AnnotationUtils;
import org.springframework.core.annotation.Order;
import org.springframework.util.Assert;
import org.springframework.web.method.support.HandlerMethodArgumentResolver;

import java.lang.reflect.Method;
import java.util.*;

@ContextOriented
@SourceType(GuiControllerService.class)
@ViewScope
@Order(2)
public class ControllerEventAdapter implements EventMappingResolver, EventMappingDispatcher {

    private static Logger logger = LoggerFactory
            .getLogger(NavigatorUtils.class);

    @Autowired
    private MethodMappingHandler methodMappingHandler;

    @Autowired
    private ApplicationContext applicationContext;

    @ContextTarget
    private GuiControllerService endpointView;

    @Track
    @SourceName("enter")
    public void enter() {
        Assert.isAssignable(HasComponents.class, endpointView.getClass());
        resolveEventMapping((HasComponents)endpointView, endpointView.getVaadinController());
    }

    protected MethodMappingHandler getMethodMappingHandler() {
        return this.methodMappingHandler;
    }

    @Override
    public void dispatchEvent(EventObject eventObject) {
        Component eventSource = (Component) eventObject.getSource();
        resolveComponentMethodParameter(eventSource);
        methodMappingHandler.handleMethodMapping(this.endpointView.getVaadinController(),
                new EventMethodsFilter(eventObject.getClass(), eventSource.getId()));
    }

    @Override
    public void resolveEventMapping(HasComponents layout, Object uiSection) {
        Set<Method> eventMappings = new LinkedHashSet<>();
        eventMappings.addAll(MethodIntrospector.selectMethods(AopUtils.getTargetClass(uiSection), EventMappingResolver.EVENT_MAPPING_METHODS));
        if(MuxContext.class.isAssignableFrom(uiSection.getClass())) {
            for(Object contextAdvice: ((MuxContext) uiSection).getJoinPointChannels()) {
                if(ContextObject.class.isAssignableFrom(contextAdvice.getClass())) {
                    eventMappings
                            .addAll(MethodIntrospector.selectMethods(((ContextObject) contextAdvice).getTargetBeanType(), EventMappingResolver.EVENT_MAPPING_METHODS));
                }
            }
        }
        for(Method eventMappingMethod: eventMappings) {
            EventMapping eventMapping = AnnotationUtils.findAnnotation(eventMappingMethod, EventMapping.class);
            Component eventSource = ComponentUtil.findComponentById(layout, eventMapping.section());

            if(AbstractClientConnector.class.isAssignableFrom(eventSource.getClass())) {
                try {
                    Method addListenerProtected = AbstractClientConnector.class
                            .getDeclaredMethod("addListener", String.class, Class.class, Object.class, Method.class);
                    Object[] params = new Object[4];
                    params[0] = eventMapping.eventId();
                    params[1] = eventMapping.event();
                    params[2] = this;
                    params[3] = this.getClass().getMethod(EventMappingDispatcher.DISPATCHER_NAME, EventObject.class);
                    AopUtils.invokeJoinpointUsingReflection(eventSource, addListenerProtected, params);
                } catch (NoSuchMethodException e) {
                    e.printStackTrace();
                } catch (Throwable throwable) {
                    throwable.printStackTrace();
                }
            }
        }
    }

    protected void resolveComponentMethodParameter(Component eventSource) {
        List<HandlerMethodArgumentResolver> argumentResolvers = new ArrayList<>();
        argumentResolvers.addAll(((MethodMappingService) methodMappingHandler).getArgumentResolvers());
        for(HandlerMethodArgumentResolver argumentResolver: argumentResolvers) {
            if(argumentResolver.getClass().isAssignableFrom(ComponentMethodArgumentResolver.class)) {
                argumentResolvers.remove(argumentResolver);
                break;
            }
        }
        argumentResolvers.add(new ComponentMethodArgumentResolver(eventSource));
        ((MethodMappingService) methodMappingHandler).setArgumentResolvers(argumentResolvers);
    }

}

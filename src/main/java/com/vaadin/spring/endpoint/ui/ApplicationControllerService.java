package com.vaadin.spring.endpoint.ui;

import com.vaadin.navigator.ViewChangeListener;
import com.vaadin.server.VaadinRequest;
import com.vaadin.server.communication.ServletUIInitHandler;
import com.vaadin.spring.annotation.SpringView;
import com.vaadin.spring.endpoint.navigator.GuiControllerService;
import com.vaadin.spring.endpoint.server.communication.UiInitInformationExchange;
import com.vaadin.spring.endpoint.util.NavigatorUtils;
import com.vaadin.ui.CssLayout;
import com.vaadin.util.CurrentInstance;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;

@SpringView(name = "")
public class ApplicationControllerService extends CssLayout implements GuiControllerService {

    private static Logger logger = LoggerFactory
            .getLogger(ApplicationControllerService.class);

    @Autowired
    private ApplicationContext applicationContext;

    @Autowired
    private UiInitInformationExchange uiInitInformationExchange;

    private String vaadinControllerName;

    private String pathInfo;


    private String[] uriVariables;

    @Override
    public Object getVaadinController() {
        return this.applicationContext.getBean(vaadinControllerName);
    }

    @Override
    public String getPathInfo() {
        return this.pathInfo;
    }

    @Override
    public String getUri() {
        return this.vaadinControllerName;
    }

    public String[] getUriVariables() {
        return uriVariables;
    }

    @Override
    public void enter(ViewChangeListener.ViewChangeEvent viewChangeEvent) {
        removeAllComponents();
        VaadinRequest request = CurrentInstance.get(VaadinRequest.class);
        this.pathInfo = ServletUIInitHandler.isUIInitRequest(request) ? request.getPathInfo() : uiInitInformationExchange.getPathInfo();
        String[] beanAndParams = NavigatorUtils
                .getVaadinControllerBeanAndParams(applicationContext, pathInfo);
        if(beanAndParams[0] == null) {
            logger.warn("No @ControllerSection for requested path [{}]", pathInfo);
            return;
        }
        this.vaadinControllerName = beanAndParams[0];
        if(beanAndParams[1] != null) {
            uriVariables = beanAndParams[1].replaceFirst("^/", "").split("/");
        }
        setSizeFull();
    }



}

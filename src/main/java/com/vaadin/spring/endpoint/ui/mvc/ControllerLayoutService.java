/*
 * Copyright 2017 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.vaadin.spring.endpoint.ui.mvc;


import com.vaadin.spring.endpoint.annotation.GuiComponent;
import com.vaadin.spring.endpoint.util.ComponentUtil;
import com.vaadin.ui.Component;
import com.vaadin.ui.ComponentContainer;
import org.contextualj.lang.weaving.MuxContext;
import org.springframework.aop.support.AopUtils;
import org.springframework.cop.scope.ContextObject;
import org.springframework.cop.support.annotation.OrderAnnotatedComparator;
import org.springframework.core.MethodIntrospector;
import org.springframework.core.annotation.AnnotationUtils;
import org.springframework.util.Assert;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 *
 * @author Octavian Stirbei
 * @since 1.0
 *
 */
public class ControllerLayoutService implements ControllerLayoutHandler, GuiComponentHandler {

    @Override
    public void handleControllerGuiComponents(ComponentContainer component, Object uiSection) {
        List<Method> htmlSections = new ArrayList<>();
        htmlSections.addAll(MethodIntrospector.selectMethods(AopUtils.getTargetClass(uiSection), HTML_SECTION_METHODS));
        if(MuxContext.class.isAssignableFrom(uiSection.getClass())) {
            for(Object contextAdvice: ((MuxContext)uiSection).getJoinPointChannels()) {
                if(ContextObject.class.isAssignableFrom(contextAdvice.getClass())) {
                    htmlSections
                            .addAll(MethodIntrospector.selectMethods(((ContextObject)contextAdvice).getTargetBeanType(), HTML_SECTION_METHODS));
                }
            }
        }
        Collections.sort(htmlSections, new OrderAnnotatedComparator());
        for(Method method: htmlSections) {
            GuiComponent htmlSection = AnnotationUtils.findAnnotation(method, GuiComponent.class);
            Assert.isAssignable(Component.class, method.getReturnType());
            Component htmlSectionComponent = null;
            try {
                if(method.getDeclaringClass().isAssignableFrom(AopUtils.getTargetClass(uiSection))) {
                    htmlSectionComponent = (Component) AopUtils.invokeJoinpointUsingReflection(uiSection, method, new Object[]{});
                } else {
                    if(MuxContext.class.isAssignableFrom(uiSection.getClass())) {
                        for(Object contextAdvice: ((MuxContext)uiSection).getJoinPointChannels()) {
                            if(ContextObject.class.isAssignableFrom(contextAdvice.getClass())) {
                                if(method.getDeclaringClass().isAssignableFrom(((ContextObject)contextAdvice).getTargetBeanType())) {
                                    htmlSectionComponent = (Component) AopUtils.invokeJoinpointUsingReflection(((ContextObject)contextAdvice).getTargetObject(), method, new Object[]{});
                                }
                            }
                        }
                    }
                }
                if(htmlSection.id().trim().length() > 0) {
                    htmlSectionComponent.setId(htmlSection.id());
                }
            } catch (Throwable throwable) {
                throwable.printStackTrace();
            }
            if(htmlSectionComponent != null) {
                handleGuiComponent(component, htmlSectionComponent, htmlSection.section());
            }

        }
    }

    @Override
    public void handleGuiComponent(ComponentContainer layout, Component htmlSectionComponent, String section) {
        if(section.trim().length() > 0) {
            Component sectionComponent = ComponentUtil.findComponentById(layout, section);
            if(sectionComponent != null) {
                Assert.isAssignable(ComponentContainer.class, sectionComponent.getClass());
                ((ComponentContainer) sectionComponent).addComponent(htmlSectionComponent);
                return;
            }
        }
        layout.addComponent(htmlSectionComponent);
    }

}

/*
 * Copyright 2015-2017 The original authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.vaadin.spring.endpoint.ui.mvc;

import com.vaadin.spring.annotation.ViewScope;
import com.vaadin.spring.endpoint.navigator.GuiControllerService;
import com.vaadin.spring.endpoint.util.NavigatorUtils;
import com.vaadin.ui.ComponentContainer;
import org.contextualj.lang.annotation.ContextTarget;
import org.contextualj.lang.annotation.expression.SourceName;
import org.contextualj.lang.annotation.expression.SourceType;
import org.contextualj.lang.annotation.pointcut.Track;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.cop.annotation.ContextOriented;
import org.springframework.core.annotation.Order;
import org.springframework.util.Assert;

@ContextOriented
@SourceType(GuiControllerService.class)
@ViewScope
@Order(1)
public class ControllerLayoutAdapter implements ControllerLayoutHandler {

    private static Logger logger = LoggerFactory
            .getLogger(NavigatorUtils.class);

    private final ControllerLayoutHandler controllerLayoutService = new ControllerLayoutService();

    private String style;

    @ContextTarget
    private GuiControllerService endpointView;

    @Track
    @SourceName("enter")
    public void enter() {
        Assert.isAssignable(ComponentContainer.class, endpointView.getClass());
        handleControllerGuiComponents((ComponentContainer)endpointView, endpointView.getVaadinController());
    }

    @Override
    public void handleControllerGuiComponents(ComponentContainer component, Object uiSection) {
        this.controllerLayoutService.handleControllerGuiComponents(component, uiSection);
    }

}

/*
 * Copyright 2015-2017 The original authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.vaadin.spring.endpoint.ui.mvc;

import com.vaadin.spring.annotation.ViewScope;
import com.vaadin.spring.endpoint.annotation.GuiController;
import com.vaadin.spring.endpoint.navigator.GuiControllerService;
import com.vaadin.spring.endpoint.ui.method.MethodMappingHandler;
import com.vaadin.spring.endpoint.util.NavigatorUtils;
import org.contextualj.lang.annotation.ContextTarget;
import org.contextualj.lang.annotation.expression.SourceName;
import org.contextualj.lang.annotation.expression.SourceType;
import org.contextualj.lang.annotation.pointcut.Track;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.aop.support.AopUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cop.annotation.ContextOriented;
import org.springframework.core.annotation.AnnotatedElementUtils;
import org.springframework.core.annotation.AnnotationAttributes;
import org.springframework.core.annotation.Order;
import org.springframework.util.AntPathMatcher;
import org.springframework.util.MultiValueMap;
import org.springframework.util.PathMatcher;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;
import org.springframework.web.servlet.HandlerMapping;
import org.springframework.web.servlet.mvc.condition.PatternsRequestCondition;
import org.springframework.web.servlet.mvc.method.RequestMappingInfo;
import org.springframework.web.util.UrlPathHelper;
import org.springframework.web.util.WebUtils;

import javax.servlet.http.HttpServletRequest;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;

import static org.springframework.web.servlet.mvc.method.annotation.RequestMappingHandlerAdapter.INIT_BINDER_METHODS;

@ContextOriented
@SourceType(GuiControllerService.class)
@ViewScope
@Order(0)
public class GuiApplicationBinderAdapter implements GuiApplicationBinderHandler {

    private static Logger logger = LoggerFactory
            .getLogger(NavigatorUtils.class);

    @Autowired
    private MethodMappingHandler methodMappingHandler;

    @ContextTarget
    private GuiControllerService endpointView;

    @Track
    @SourceName("enter")
    public void enter() {
        handleInitBinder(endpointView.getVaadinController());
    }

    @Override
    public void handleInitBinder(Object uiSection) {
        AnnotationAttributes aa = AnnotatedElementUtils
                .getMergedAnnotationAttributes(AopUtils.getTargetClass(uiSection), GuiController.class);
        resolveBinderRequestAttributes(aa.getString("uri"), ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getRequest());
        this.methodMappingHandler.handleMethodMapping(uiSection, INIT_BINDER_METHODS);
    }

    protected void resolveBinderRequestAttributes(String lookupPath, HttpServletRequest request) {
        RequestMappingInfo.BuilderConfiguration options = new RequestMappingInfo.BuilderConfiguration();
        UrlPathHelper urlPathHelper = new UrlPathHelper();
        PathMatcher pathMatcher = new AntPathMatcher();
        PatternsRequestCondition patternsCondition = new PatternsRequestCondition(
                new String[] { lookupPath }, urlPathHelper, pathMatcher,
                options.useSuffixPatternMatch(), options.useTrailingSlashMatch(),
                options.getFileExtensions());

        String bestPattern;
        Map<String, String> uriVariables;
        Map<String, String> decodedUriVariables;

        Set<String> patterns =  patternsCondition.getPatterns();
        if (patterns.isEmpty()) {
            bestPattern = lookupPath;
            uriVariables = Collections.emptyMap();
            decodedUriVariables = Collections.emptyMap();
        }
        else {
            bestPattern = patterns.iterator().next();
            uriVariables = pathMatcher.extractUriTemplateVariables(bestPattern, endpointView.getPathInfo());
            decodedUriVariables = urlPathHelper.decodePathVariables(request, uriVariables);
        }

        request.setAttribute(HandlerMapping.BEST_MATCHING_PATTERN_ATTRIBUTE, bestPattern);
        request.setAttribute(HandlerMapping.URI_TEMPLATE_VARIABLES_ATTRIBUTE, decodedUriVariables);

        Map<String, MultiValueMap<String, String>> matrixVars =
                extractMatrixVariables(request, uriVariables, urlPathHelper);
        request.setAttribute(HandlerMapping.MATRIX_VARIABLES_ATTRIBUTE, matrixVars);
    }

    private Map<String, MultiValueMap<String, String>> extractMatrixVariables(
            HttpServletRequest request, Map<String, String> uriVariables, UrlPathHelper urlPathHelper) {

        Map<String, MultiValueMap<String, String>> result = new LinkedHashMap<String, MultiValueMap<String, String>>();
        for (Map.Entry<String, String> uriVar : uriVariables.entrySet()) {
            String uriVarValue = uriVar.getValue();

            int equalsIndex = uriVarValue.indexOf('=');
            if (equalsIndex == -1) {
                continue;
            }

            String matrixVariables;

            int semicolonIndex = uriVarValue.indexOf(';');
            if ((semicolonIndex == -1) || (semicolonIndex == 0) || (equalsIndex < semicolonIndex)) {
                matrixVariables = uriVarValue;
            }
            else {
                matrixVariables = uriVarValue.substring(semicolonIndex + 1);
                uriVariables.put(uriVar.getKey(), uriVarValue.substring(0, semicolonIndex));
            }

            MultiValueMap<String, String> vars = WebUtils.parseMatrixVariables(matrixVariables);
            result.put(uriVar.getKey(), urlPathHelper.decodeMatrixVariables(request, vars));
        }
        return result;
    }

}

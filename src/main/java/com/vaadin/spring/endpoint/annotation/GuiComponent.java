/*
 * Copyright 2017 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.vaadin.spring.endpoint.annotation;

import com.vaadin.ui.Component;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.AliasFor;
import org.springframework.core.annotation.Order;

import java.lang.annotation.*;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * Marker annotation
 *
 * Indicates that method should be section content of the <i>component</i>
 * and should return {@link Component}
 *
 * @author Octavian Stirbei
 * @since 1.0
 *
 */
@Target({ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
@Order
@Documented
public @interface GuiComponent {

    AtomicInteger INC_ID = new AtomicInteger(Ordered.LOWEST_PRECEDENCE);

    String id() default "";

    String section() default "";

    @AliasFor(annotation = Order.class, attribute = "value")
    int renderOrder() default Ordered.LOWEST_PRECEDENCE;

}

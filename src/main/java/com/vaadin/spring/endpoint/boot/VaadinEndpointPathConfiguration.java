package com.vaadin.spring.endpoint.boot;

import com.vaadin.spring.boot.internal.VaadinServletConfiguration;
import com.vaadin.spring.endpoint.util.NavigatorUtils;
import org.aopalliance.intercept.MethodInvocation;
import org.contextualj.lang.annotation.expression.SourceName;
import org.contextualj.lang.annotation.expression.SourceType;
import org.contextualj.lang.annotation.pointcut.Wrap;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.cop.annotation.ContextOriented;
import org.springframework.web.servlet.handler.SimpleUrlHandlerMapping;
import org.springframework.web.servlet.mvc.Controller;

import java.util.Map;

@ContextOriented
@SourceType(VaadinServletConfiguration.class)
public class VaadinEndpointPathConfiguration {

    private static Logger logger = LoggerFactory
            .getLogger(VaadinEndpointPathConfiguration.class);

    @Autowired
    private ApplicationContext applicationContext;

    @Autowired
    private Controller vaadinUiForwardingController;


    @Wrap
    @SourceName("vaadinUiForwardingHandlerMapping")
    public SimpleUrlHandlerMapping vaadinUiForwardingHandlerMapping(MethodInvocation joinPoint) throws Throwable {
        SimpleUrlHandlerMapping mapping = (SimpleUrlHandlerMapping) joinPoint.proceed();
        Map<String, Object> urlMappings = (Map<String, Object>) mapping.getUrlMap();
        for (String path : NavigatorUtils.getVaadinControllerUris(this.applicationContext)) {
            urlMappings.put("/" + path,  vaadinUiForwardingController);
            if (path.length() > 0) {
                urlMappings.put("/" + path + "**",
                        vaadinUiForwardingController);
            }
        }
        logger.info("Forwarding @VaadinController URLs from [{}]", mapping.getUrlMap());
        mapping.setUrlMap(urlMappings);
        return mapping;
    }

}

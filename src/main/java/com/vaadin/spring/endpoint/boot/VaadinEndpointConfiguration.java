package com.vaadin.spring.endpoint.boot;

import com.vaadin.server.VaadinServlet;
import com.vaadin.spring.endpoint.server.EndpointVaadinServlet;
import com.vaadin.spring.endpoint.ui.method.MethodMappingHandler;
import com.vaadin.spring.endpoint.ui.method.MethodMappingService;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan("com.vaadin.spring.endpoint")
public class VaadinEndpointConfiguration {

    @Bean
    public MethodMappingHandler getMethodMappingHandler() {
        return new MethodMappingService();
    }

    @Bean
    public VaadinServlet vaadinServlet() {
        return new EndpointVaadinServlet();
    }

}
